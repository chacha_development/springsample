package kr.chachacreation.admin.controller;

import kr.co.nicepay.module.lite.NicePayWebConnector;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping
public class AdminController {
	private final String mEncodeKey = "RRMLc0qrxIu+LTZjapK7BkzwhcPwNHKYElIScN3KByr5k5zHWfHTd9bNT3Rg9LoPYofZSwGEFMlxgVYMqI/gHA==";
	private final String mNicePayHome = "C:\\Users\\kimkisun\\repos\\SpringSample\\lib\\";
	private final String mMid = "chachacrem";
	private final String mCancelPwd = "carshare17";
	private final String mGoodsCall = "차차호출서비스";
	private final String mGoodsReservation = "차차예약서비스";
	private final String mCancelCall = "호출취소";
	private final String mCancelReservation = "예약취소";
	private final String mMallIP = "127.0.0.1";
	private final String mCurrency = "KRW";
	private final String mServiceCall = "E";
	private final String mServiceReservation = "RE";

	@GetMapping(value = {"/", "/main", "/main.html", "/home", "index.html"})
	public String main() {

		return "index";
	}

	@PostMapping(value = {"/", "/main", "/main.html", "/home", "index.html"})
	public String test() {

		return "index";
	}

	@PostMapping(value = "rest")
	@ResponseBody
	public Map<String, Object> rest() {

		Map<String, Object> result = new HashMap<>();
		result.put("name", "kim");

		return result;
	}

	@PostMapping(value = "nicepay_pay")
	@ResponseBody
	public Map<String, Object> nicepay_pay(@RequestBody Map<String, Object> param) {
		int useq = (int)param.get("useq");									// USER.useq
		int cseq = (int)param.get("cseq");									// USER_PAYMENT.cseq
		int rseq = (int)param.get("rseq");									// 예약테이블.식별자
		int amount = (int)param.get("amount");								// 결제 금액
		int service = (int)param.get("service");							// 서비스 구분 (0: 일반호출, 1: 예약)

        // select from DB
		String pay_nonce = "BIKYchachacrem2001242112003050";						// USER_PAYMENT.pay_nonce  (select pay_nonce from user_payment where cseq = ..)
		String order_number = String.format("%s%06d", mServiceReservation, rseq);	// 서비스구분+예약번호 (ex. RE + rseq)
		String uname = "김기선";														// USER.name
		String uid = "kskim.chacha@gmail.com";										// USER.uid
		String goodsName = "";
		if (service == 1) {
		    goodsName = mGoodsReservation;
		} else {
		    goodsName = mGoodsCall;
		}

		NicePayWebConnector	connector;
		try {
			connector = new NicePayWebConnector();
			connector.addRequestData("EncodeKey", mEncodeKey);				// 상점키
			connector.setNicePayHome(mNicePayHome);							// nicepay.properties 경로
			connector.addRequestData("BillKey", pay_nonce);					// 빌키
			connector.addRequestData("MID", mMid);							// 상점 아이디
			connector.addRequestData("Moid", order_number);					// 주문번호
			connector.addRequestData("Amt", Integer.toString(amount));		// 결제 금액
			connector.addRequestData("GoodsName", goodsName);				// 상품명
			connector.addRequestData("BuyerName", uname);					// 구매자
			connector.addRequestData("BuyerEmail", uid);					// 구매자 E-Mail
			connector.addRequestData("MallIP", mMallIP);					// 몰 IP
			connector.addRequestData("Currency", mCurrency);				// 통화코드 (원화: KRW, 외화: USD)
			connector.addRequestData("CardInterest", "0");					// 무이자 여부 (0: 일반, 1:무이자)
			connector.addRequestData("CardQuota", "00");                    // 할부 개월수 (일시불: 00, 2개월: 02)
			connector.addRequestData("actionType", "PY0");                  // 고정값, 수정금지
			connector.addRequestData("PayMethod", "BILL");                  // 고정값, 수정금지
			connector.requestAction();

			String resultCode = connector.getResultData("ResultCode");	// 결과코드 (성공 :3001 ,이외 실패)
			String resultMsg = connector.getResultData("ResultMsg");	// 결과 메시지
			String tid = connector.getResultData("TID");				// 거래 아이디
			String authDate = connector.getResultData("AuthDate");		// 승인 날짜
			String authCode = connector.getResultData("AuthCode");		// 승인 번호
			String cardCode = connector.getResultData("CardCode");		// 카드사코드
			String cardName = connector.getResultData("CardName");		// 카드사명
			String cardNo = connector.getResultData("CardNo");			// 카드번호

			Map<String, Object> result = new HashMap<>();
			if (resultCode.compareTo("3001") == 0) {
				// update to DB (payment success)
				// ..

				result.put("rlt", "1");
				result.put("error", "결제가 완료되었습니다.");
				result.put("ResultCode", resultCode);
				result.put("ResultMsg", resultMsg);
			} else {
				// update to DB (payment failed)
				// ..

				result.put("rlt", "-1");
				result.put("error", "결제에 실패하였습니다. ");
				result.put("ResultCode", resultCode);
				result.put("ResultMsg", resultMsg);
			}

			//System.out.println(resultMsg + "["+resultCode+"]");
			return result;

		} catch (Exception e) {
			System.out.println(e.getMessage());

			Map<String, Object> result = new HashMap<>();
			result.put("rlt", "-9999");
			result.put("error", "올바르지 않은 결제 요청입니다.");
			result.put("message", e.getMessage());
			return result;
		}
	}

	@PostMapping(value = "nicepay_cancel")
	@ResponseBody
	public Map<String, Object> nicepay_cancel(@RequestBody Map<String, Object> param) {
		int useq = (int)param.get("useq");									// USER.useq
		int rseq = (int)param.get("rseq");									// 예약테이블.식별자
		int amount = (int)param.get("amount");								// 취소 금액
		int service = (int)param.get("service");							// 서비스 구분 (0: 일반호출, 1: 예약)
		int partial = (int)param.get("partial");							// 취소 구분 (0: 전체취소, 1: 부분취소)

		// select from DB
		String tid = "chachacrem01162008250921033251";						// 거래 아이디 (결제시 수신한 값, 예약결제테이블.tid)
		String cancelReason = "";
		if (service == 1) {
			cancelReason = mCancelReservation;
		} else {
			cancelReason = mCancelCall;
		}
		String partialCode = "";
		if (partial == 1) {
			partialCode = "1";
		} else {
			partialCode = "0";
		}

		NicePayWebConnector	connector;
		try {
			connector = new NicePayWebConnector();
			connector.addRequestData("EncodeKey", mEncodeKey);				// 상점키
			connector.setNicePayHome(mNicePayHome);							// nicepay.properties 경로
			connector.addRequestData("MID", mMid);							// 상점 아이디
			connector.addRequestData("TID", tid);							// 거래 아이디 (결제시 수신값)
			connector.addRequestData("CancelAmt", Integer.toString(amount));// 취소 금액
			connector.addRequestData("CancelMsg", cancelReason);			// 취소 사유
			connector.addRequestData("CancelPwd", mCancelPwd);				// 취소 비밀번호
			connector.addRequestData("PartialCancelCode", partialCode);		// 부분취소 여부 (0: 전체취소, 1: 부분취소)
			connector.addRequestData("actionType", "CL0");                  // 서비스모드 (CL0: 취소)
			connector.addRequestData("CancelIP", mMallIP);                  // 취소 IP
			connector.requestAction();

			String resultCode = connector.getResultData("ResultCode");	// 결과코드 (취소성공 :2001 ,이외 실패)
			String resultMsg = connector.getResultData("ResultMsg");	// 결과 메세지
			String cancelAmt = connector.getResultData("CancelAmt");	// 취소 금액
			String cancelDate = connector.getResultData("CancelDate");	// 취소 일자
			String cancelTime = connector.getResultData("CancelTime");	// 취소 시간
			String cancelNum = connector.getResultData("CancelNum");	// 취소 번호
			String payMethod = connector.getResultData("PayMethod");	// 취소 수단
			String resultMid = connector.getResultData("MID");			// MID
			String resultTid = connector.getResultData("TID");			// TID
			String errorCD = connector.getResultData("ErrorCD");		// 에러 코드
			String errorMsg = connector.getResultData("ErrorMsg");		// 에러 메세지
			String authDate = connector.getResultData("AuthDate");		// 거래 시간

			Map<String, Object> result = new HashMap<>();
			if (resultCode.compareTo("2001") == 0) {
				// update to DB (success)
				// ..

				result.put("rlt", "1");
				result.put("error", "결제 취소가 완료되었습니다.");
				result.put("ResultCode", resultCode);
				result.put("ResultMsg", resultMsg);
				result.put("CancelAmt", cancelAmt);
				result.put("CancelDate", cancelDate);
				result.put("CancelTime", cancelTime);
				result.put("CancelNum", cancelNum);
				result.put("PayMethod", payMethod);
				result.put("MID", resultMid);
				result.put("TID", resultTid);
				result.put("ErrorCD", errorCD);
				result.put("ErrorMsg", errorMsg);
				result.put("AuthDate", authDate);
			} else {
				// update to DB (failed)
				// ..

				result.put("rlt", "-1");
				result.put("error", "결제 취소에 실패하였습니다.");
				result.put("ResultCode", resultCode);
				result.put("ResultMsg", resultMsg);
				result.put("ErrorCD", errorCD);
				result.put("ErrorMsg", errorMsg);
			}

			//System.out.println(resultMsg + "["+resultCode+"]");
			return result;

		} catch (Exception e) {
			System.out.println(e.getMessage());

			Map<String, Object> result = new HashMap<>();
			result.put("rlt", -9999);
			result.put("error", "올바르지 않은 결제 취소 요청입니다.");
			result.put("message", e.getMessage());
			return result;
		}
	}
}
